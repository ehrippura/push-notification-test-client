//
//  TokenService.m
//  PushNotificationClient
//
// Copyright (c) 2015 Tzu-Yi Lin
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "TokenService.h"
#import "Device.h"

NSString *DeviceAddNotification = @"DeviceAddNotification";

@implementation TokenService

+ (instancetype)sharedService
{
    static id master = nil;
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        master = [[self alloc] init];
    });

    return master;
}

- (NSString *)storagePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
    NSString *path = paths[0];

    return [path stringByAppendingPathComponent:@"tokens"];
}

- (id)init
{
    self = [super init];
    if (self) {
        NSFileManager *fm = [NSFileManager defaultManager];
        NSString *path = [self storagePath];

        if ([fm fileExistsAtPath:path]) {
            @try {
                _devices = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
            }
            @catch (NSException *exception) {
                _devices = [[NSMutableArray alloc] init];
            }

            if (_devices == nil) {
                _devices = [[NSMutableArray alloc] init];
            }

        } else {
            _devices = [[NSMutableArray alloc] init];
        }
    }
    return self;
}

- (void)save
{
    NSString *path = [self storagePath];
    [NSKeyedArchiver archiveRootObject:_devices toFile:path];
}

- (void)addDeviceToken:(NSString *)token
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", @"[a-zA-Z0-9]{64}"];
    if ([predicate evaluateWithObject:token]) {
        Device *device = [[Device alloc] init];
        device.token = token;
        [_devices addObject:device];
        [self save];
    }
}

- (void)insertObject:(Device *)object inDevicesAtIndex:(NSUInteger)index
{
    [self.devices insertObject:object atIndex:index];
    [[NSNotificationCenter defaultCenter] postNotificationName:DeviceAddNotification object:nil];
}

- (void)removeObjectFromDevicesAtIndex:(NSUInteger)index
{
    [self.devices removeObjectAtIndex:index];
    [[NSNotificationCenter defaultCenter] postNotificationName:DeviceAddNotification object:nil];
}

@end
