//
//  main.m
//  
//
//  Created by Wayne Lin on 2015/5/14.
//  Copyright (c) 2015年 Tzu-Yi Lin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
