//
//  SecurityService.h
//  
//
// Copyright (c) 2015 Tzu-Yi Lin
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import <Foundation/Foundation.h>
#include <Security/Security.h>

@interface SecurityService : NSObject {

    NSInputStream *inputStream;
    NSOutputStream *outputStream;
    dispatch_queue_t networkQueue;

    // security
    SecCertificateRef certificate;
    SecIdentityRef identity;
}

@property (nonatomic, copy, readonly) NSString *host;
@property (nonatomic, assign, readonly) NSUInteger port;

@property (atomic, getter=isOpened) BOOL opened;

+ (instancetype)serviceForTarget:(NSString *)target port:(NSUInteger)port;

- (id)initWithTarget:(NSString *)target port:(NSUInteger)port;
- (BOOL)setCertificateFile:(NSString *)filePath passphrase:(NSString *)passphrase;

- (void)open;
- (void)close;

- (void)sendData:(NSData *)data;

@end
