//
//  APNPayload.m
//  PushNotificationClient
//
// Copyright (c) 2015 Tzu-Yi Lin
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "APNPayload.h"

#define DEVICE_TOKEN_SIZE 32
#define MAX_PAYLOAD_LENGTH  2048

@interface APNPayload() {
    NSData *_data;
}

@end

@implementation APNPayload

- (NSData *)decodeFromHexidecimal:(NSString *)hexString
{
    unsigned char (^strToChar)(char a, char b) = ^unsigned char (char a, char b){
        char encoder[3] = {'\0','\0','\0'};
        encoder[0] = a;
        encoder[1] = b;
        return (char) strtol(encoder,NULL,16);
    };

    const char * bytes = [hexString cStringUsingEncoding:NSUTF8StringEncoding];
    NSUInteger length = strlen(bytes);
    unsigned char * r = (unsigned char *) malloc(length / 2 + 1);
    unsigned char * index = r;

    while ((*bytes) && (*(bytes +1))) {
        *index = strToChar(*bytes, *(bytes +1));
        index++;
        bytes+=2;
    }
    *index = '\0';

    NSData * result = [NSData dataWithBytes: r length: length / 2];
    free(r);

    return result;
}

- (id)initWithPayloadContent:(NSString *)content deviceToken:(NSString *)token
{
    if ([content length] == 0 || [token length] != DEVICE_TOKEN_SIZE * 2)
        return nil;

    self = [super init];
    if (self) {
        [self processContent:content token:token];
    }
    return self;
}

- (void)processContent:(NSString *)content token:(NSString *)token
{
    static uint32_t count = 0;
    NSMutableData *resultData = [[NSMutableData alloc] init];
    NSMutableData *frameData = [[NSMutableData alloc] init];

    void (^packFrame)(uint8_t, NSData *) = ^(uint8_t itemID, NSData *data) {
        [frameData appendBytes:&itemID length:sizeof(itemID)];
        uint16_t length = htons((uint16_t)[data length]);
        [frameData appendBytes:&length length:sizeof(uint16_t)];
        [frameData appendData:data];
    };

    // append command
    uint8_t command = 2;
    [resultData appendBytes:&command length:sizeof(uint8_t)];

    // append token
    packFrame(1, [self decodeFromHexidecimal:token]);

    // append content
    NSData *contentData = [content dataUsingEncoding:NSUTF8StringEncoding];
    packFrame(2, contentData);

    // append identifier
    if (++count == UINT32_MAX) {
        count = 0;
    }
    NSData *identifierData = [NSData dataWithBytes:&count length:sizeof(uint32_t)];
    packFrame(3, identifierData);

    // append frame
    uint32_t frameLength = htonl((uint32_t)[frameData length]);
    [resultData appendBytes:&frameLength length:sizeof(uint32_t)];
    [resultData appendData:frameData];

    _data = [resultData copy];
}

- (NSData *)data
{
    return _data;
}

@end
