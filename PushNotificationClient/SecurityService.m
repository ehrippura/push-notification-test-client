//
//  SecurityService.m
//  
//
// Copyright (c) 2015 Tzu-Yi Lin
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
#import "SecurityService.h"

@interface SecurityService () <NSStreamDelegate> {
    BOOL networkKeepsRunning;
    NSMutableData *incomingData;
    NSMutableData *outgoingData;
    NSLock *outgoingLock;
}
@end

@implementation SecurityService

+ (instancetype)serviceForTarget:(NSString *)target port:(NSUInteger)port
{
    return [[self alloc] initWithTarget:target port:port];
}

- (id)initWithTarget:(NSString *)target port:(NSUInteger)port
{
    self = [super init];
    if (self) {
        _host = [target copy];
        _port = port;
        self.opened = NO;

        certificate = NULL;
    }

    return self;
}

- (void)dealloc
{
    if (self.opened) {
        [self close];
    }

    if (certificate) {
        CFRelease(certificate);
    }

    if (identity) {
        CFRelease(identity);
    }
}

- (BOOL)setCertificateFile:(NSString *)filePath passphrase:(NSString *)passphrase
{
    if (certificate) {
        CFRelease(certificate);
    }

    if (identity) {
        CFRelease(identity);
    }

    NSData *certificateData = [NSData dataWithContentsOfFile:filePath];

    NSDictionary *options = @{
        (__bridge id)kSecImportExportPassphrase: passphrase,
    };

    CFArrayRef resultArray = NULL;
    OSStatus status = SecPKCS12Import((__bridge CFDataRef)certificateData, (__bridge CFMutableDictionaryRef)options, &resultArray);

    if (status == errSecSuccess) {
        NSLog(@"Open security file success. Item: %ld", CFArrayGetCount(resultArray));
        CFDictionaryRef identityDictionary = CFArrayGetValueAtIndex(resultArray, 0);

        identity = (SecIdentityRef) CFRetain(CFDictionaryGetValue(identityDictionary, kSecImportItemIdentity));
        status = SecIdentityCopyCertificate(identity, &certificate);

        return status == errSecSuccess;
    }

    return NO;
}

- (void)createStream
{
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;

    CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault, (__bridge CFStringRef)self.host, (UInt32)self.port, &readStream, &writeStream);

    inputStream = CFBridgingRelease(readStream);
    outputStream = CFBridgingRelease(writeStream);

    NSAssert([inputStream streamStatus] != NSStreamStatusError, @"Input Stream Creation Error");
    NSAssert([outputStream streamStatus] != NSStreamStatusError, @"Output Stream Creation Error");

    [inputStream setDelegate:self];
    [outputStream setDelegate:self];

    NSDictionary *securitySettings = @{
        (__bridge id)kCFStreamSSLLevel: (__bridge id)kCFStreamSocketSecurityLevelTLSv1,
        (__bridge id)kCFStreamSSLCertificates: @[(__bridge id)identity, (__bridge id)certificate],
    };

    [inputStream setProperty:securitySettings forKey:(__bridge id)kCFStreamPropertySSLSettings];
    [outputStream setProperty:securitySettings forKey:(__bridge id)kCFStreamPropertySSLSettings];
}

- (void)open
{
    networkQueue = dispatch_queue_create("security object runloop", 0);
    [self createStream];
    [self threadBody];
}

- (void)threadBody
{
    networkKeepsRunning = YES;
    outgoingLock = [[NSLock alloc] init];

    dispatch_async(networkQueue, ^{

        NSRunLoop *networkRunLoop = [NSRunLoop currentRunLoop];

        [self->inputStream scheduleInRunLoop:networkRunLoop forMode:NSDefaultRunLoopMode];
        [self->outputStream scheduleInRunLoop:networkRunLoop forMode:NSDefaultRunLoopMode];
        [self->inputStream open];
        [self->outputStream open];

        self.opened = YES;
        
        while (self->networkKeepsRunning && [networkRunLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]])
            ;

        [self->inputStream close];
        [self->outputStream close];
        [self->inputStream removeFromRunLoop:networkRunLoop forMode:NSDefaultRunLoopMode];
        [self->outputStream removeFromRunLoop:networkRunLoop forMode:NSDefaultRunLoopMode];

        self.opened = NO;

        dispatch_async(dispatch_get_main_queue(), ^{
            self->inputStream = nil;
            self->outputStream = nil;
            self->outgoingData = nil;
            self->incomingData = nil;
            self->networkQueue = nil;
            self->outgoingLock = nil;
        });
    });
}

- (void)close
{
    networkKeepsRunning = NO;
}

- (void)sendData:(NSData *)data
{
    if (!outgoingData) {
        outgoingData = [[NSMutableData alloc] init];
    }

    [outgoingLock lock];

    NSInteger result = [outputStream write:[data bytes] maxLength:[data length]];

    if (result < 0) {
        [outgoingData appendData:data];
    } else if (result < [data length]) {
        [outgoingData appendBytes:([data bytes] + result) length:[data length] - result];
    }

    [outgoingLock unlock];
}

// stream delegate
- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode
{
    switch (eventCode) {
        case NSStreamEventOpenCompleted:
            NSLog(@"%@ open complete", aStream);
            break;

        case NSStreamEventHasBytesAvailable:
            if (aStream == inputStream) {
                if (!incomingData) {
                    incomingData = [[NSMutableData alloc] init];
                }

                const NSUInteger max = 1024;
                uint8 dataBuffer[max] = {0};
                NSInteger readSize = [inputStream read:dataBuffer maxLength:max];

                [incomingData appendBytes:dataBuffer length:readSize];
            }
            break;

        case NSStreamEventHasSpaceAvailable:
            NSLog(@"Output Has Space");
            if (aStream == outputStream) {
                [outgoingLock lock];
                if ([outgoingData length] > 0) {
                    const uint8_t *ptr = [outgoingData bytes];
                    NSUInteger length = [outgoingData length];
                    NSInteger result = [outputStream write:ptr maxLength:length];
                    [outgoingData replaceBytesInRange:NSMakeRange(0, result) withBytes:NULL length:0];
                }
                [outgoingLock unlock];
            }
            break;

        case NSStreamEventEndEncountered:
            // if received response means error occurred
            // close connection
            NSLog(@"Input: %@", incomingData);
            incomingData = nil;
            [self close];

            break;

        case NSStreamEventErrorOccurred:
            NSLog(@"Error Occured");
            break;

        case NSStreamEventNone:
            break;
    }
}

@end
