//
//  AppController.m
//  PushNotificationClient
//
// Copyright (c) 2015 Tzu-Yi Lin
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "AppController.h"
#import "SecurityService.h"
#import "APNPayload.h"
#import "TokenPanelController.h"
#import "TokenService.h"
#import "Device.h"

#define SANDBOX_STATE @"sandbox.state"

@interface AppController () {
    TokenPanelController *panelController;
}

@end

@implementation AppController

- (void)awakeFromNib
{
    [self.payloadTextView setAutomaticQuoteSubstitutionEnabled:NO];
    [self.payloadTextView setAutomaticSpellingCorrectionEnabled:NO];
    [self.payloadTextView setAutomaticTextReplacementEnabled:NO];

    // load default text
    NSString *path = [[NSBundle mainBundle] pathForResource:@"defaultPayload" ofType:@"plist"];
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];

    [self.payloadTextView setString:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]];
    [self.sandbox setState:[[NSUserDefaults standardUserDefaults] integerForKey:SANDBOX_STATE]];
    [self updateService];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateDeviceCount:) name:DeviceAddNotification object:nil];

    [self updateDeviceCount:nil];
}

- (void)updateDeviceCount:(id)sender
{
    [self.deviceCountLabel setStringValue:[NSString stringWithFormat:@"%ld Devices", [TokenService sharedService].devices.count]];
}

- (void)updateService
{
    [self.sendButton unbind:NSEnabledBinding];
    [self.closeButton unbind:NSEnabledBinding];
    [self.certificateLabel setStringValue:@""];

    if ([self.sandbox state] == NSOnState) {
        service = [SecurityService serviceForTarget:@"gateway.sandbox.push.apple.com" port:2195];
    } else {
        service = [SecurityService serviceForTarget:@"gateway.push.apple.com" port:2195];
    }

    [self.sendButton bind:NSEnabledBinding toObject:service withKeyPath:@"opened" options:nil];
    [self.closeButton bind:NSEnabledBinding toObject:service withKeyPath:@"opened" options:nil];
}

- (IBAction)openFile:(id)sender
{
    NSOpenPanel *panel = [NSOpenPanel openPanel];

    panel.canChooseDirectories = NO;
    panel.allowsMultipleSelection = NO;
    panel.allowedFileTypes = @[@"p12"];
    panel.accessoryView = self.passView;
    panel.delegate = self;

    if (@available(macOS 10.11, *)) {
        panel.accessoryViewDisclosed = YES;
    }

    __weak NSOpenPanel *weakPanel = panel;
    [panel beginWithCompletionHandler:^(NSInteger result) {

        if (result == NSModalResponseOK) {
            NSURL *fileurl = weakPanel.URL;

            if ([self->service setCertificateFile:fileurl.path passphrase:[self.passphraseField stringValue]]) {
                [self.certificateLabel setStringValue:fileurl.lastPathComponent];
            } else {
                NSLog(@"Error");
            }
        }
    }];
}

- (IBAction)openConnectionButtonDown:(id)sender
{
    [service open];
}

- (IBAction)closeConnectionButtonDown:(id)sender
{
    [service close];
}

- (IBAction)sendData:(id)sender
{
    BOOL isSandbox = [self.sandbox state] == NSOnState;
    NSArray *devices = [[TokenService sharedService].devices filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"sandbox == %@", @(isSandbox)]];

    for (Device *device in devices) {

        APNPayload *payload = [[APNPayload alloc] initWithPayloadContent:[self.payloadTextView string]
                                                             deviceToken:device.token];

        if (payload) {
            [service sendData:payload.data];
        }
    }
}

- (IBAction)sandboxChanged:(id)sender
{
    [self updateService];
    [[NSUserDefaults standardUserDefaults] setInteger:[self.sandbox state] forKey:SANDBOX_STATE];
}

- (IBAction)openTokenWindow:(id)sender
{
    if (!panelController) {
        panelController = [[TokenPanelController alloc] initWithWindowNibName:@"TokenPanel"];
    }
    [panelController showWindow:sender];
}

- (BOOL)panel:(id)sender validateURL:(NSURL *)url error:(NSError **)outError
{
    return [[self.passphraseField stringValue] length] > 0;
}

@end
